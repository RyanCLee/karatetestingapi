$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("RequestToken.feature");
formatter.feature({
  "line": 2,
  "name": "",
  "description": "",
  "id": "",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@ignore"
    }
  ]
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "url \u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
  "keyword": "* "
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 1148840173,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Return the token",
  "description": "",
  "id": ";return-the-token",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 9,
  "name": "url \u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "def payload \u003d",
  "keyword": "* ",
  "doc_string": {
    "content_type": "",
    "line": 11,
    "value": "\u003csoapenv:Envelope xmlns:soapenv\u003d\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws\u003d\"http://ws.openkm.com\"\u003e\n   \u003csoapenv:Header/\u003e\n   \u003csoapenv:Body\u003e\n      \u003cws:login\u003e\n         \u003c!--Optional:--\u003e\n         \u003cuser\u003eTestUser\u003c/user\u003e\n         \u003c!--Optional:--\u003e\n         \u003cpassword\u003eTestUser\u003c/password\u003e\n      \u003c/ws:login\u003e\n   \u003c/soapenv:Body\u003e\n\u003c/soapenv:Envelope\u003e"
  }
});
formatter.step({
  "line": 24,
  "name": "request payload",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "soap action \u0027http://ws.openkm.com/OKMAuth/login\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "status 200",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "def result \u003d //return",
  "keyword": "* "
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 11627867,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "payload",
      "offset": 4
    }
  ],
  "location": "StepDefs.defDocString(String,String)"
});
formatter.result({
  "duration": 9830649,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "payload",
      "offset": 8
    }
  ],
  "location": "StepDefs.request(String)"
});
formatter.result({
  "duration": 267840,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": " \u0027http://ws.openkm.com/OKMAuth/login\u0027",
      "offset": 11
    }
  ],
  "location": "StepDefs.soapAction(String)"
});
formatter.result({
  "duration": 499369127,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 7
    }
  ],
  "location": "StepDefs.status(int)"
});
formatter.result({
  "duration": 1042848,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "result",
      "offset": 4
    },
    {
      "val": "//return",
      "offset": 13
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 2164585,
  "status": "passed"
});
formatter.uri("sample.feature");
formatter.feature({
  "line": 2,
  "name": "",
  "description": "",
  "id": "",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@ignore"
    }
  ]
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "url \u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
  "keyword": "* "
});
formatter.step({
  "line": 6,
  "name": "def creator \u003d read(\u0027RequestToken.feature\u0027)",
  "keyword": "* "
});
formatter.step({
  "comments": [
    {
      "line": 7,
      "value": "#  * def result \u003d callonce creator"
    }
  ],
  "line": 8,
  "name": "def result1 \u003d call creator",
  "keyword": "* "
});
formatter.step({
  "line": 9,
  "name": "def x \u003d result1.result",
  "keyword": "* "
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 35860893,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "creator",
      "offset": 4
    },
    {
      "val": "read(\u0027RequestToken.feature\u0027)",
      "offset": 14
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 30335221,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "result1",
      "offset": 4
    },
    {
      "val": "call creator",
      "offset": 14
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 72611925,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "x",
      "offset": 4
    },
    {
      "val": "result1.result",
      "offset": 8
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 50827019,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Check if the folder can be read",
  "description": "",
  "id": ";check-if-the-folder-can-be-read",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 12,
  "name": "url \u0027http://localhost:8000/OpenKM/services/OKMFolder?wsdl\u0027",
  "keyword": "Given "
});
formatter.step({
  "line": 13,
  "name": "def payload \u003d",
  "keyword": "* ",
  "doc_string": {
    "content_type": "",
    "line": 14,
    "value": "\u003csoapenv:Envelope xmlns:soapenv\u003d\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws\u003d\"http://ws.openkm.com\"\u003e\n   \u003csoapenv:Header/\u003e\n   \u003csoapenv:Body\u003e\n      \u003cws:isValid\u003e\n         \u003c!--Optional:--\u003e\n         \u003ctoken\u003e#(x)\u003c/token\u003e\n         \u003c!--Optional:--\u003e\n         \u003cfldPath\u003e/okm:root/ReadOnly\u003c/fldPath\u003e\n      \u003c/ws:isValid\u003e\n   \u003c/soapenv:Body\u003e\n\u003c/soapenv:Envelope\u003e"
  }
});
formatter.step({
  "line": 27,
  "name": "request payload",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "soap action \u0027http://ws.openkm.com/OKMFolder/isValid\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "status 200",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027http://localhost:8000/OpenKM/services/OKMFolder?wsdl\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 12521367,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "payload",
      "offset": 4
    }
  ],
  "location": "StepDefs.defDocString(String,String)"
});
formatter.result({
  "duration": 21281425,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "payload",
      "offset": 8
    }
  ],
  "location": "StepDefs.request(String)"
});
formatter.result({
  "duration": 54619,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": " \u0027http://ws.openkm.com/OKMFolder/isValid\u0027",
      "offset": 11
    }
  ],
  "location": "StepDefs.soapAction(String)"
});
formatter.result({
  "duration": 32460731,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 7
    }
  ],
  "location": "StepDefs.status(int)"
});
formatter.result({
  "duration": 85091,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "url \u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
  "keyword": "* "
});
formatter.step({
  "line": 6,
  "name": "def creator \u003d read(\u0027RequestToken.feature\u0027)",
  "keyword": "* "
});
formatter.step({
  "comments": [
    {
      "line": 7,
      "value": "#  * def result \u003d callonce creator"
    }
  ],
  "line": 8,
  "name": "def result1 \u003d call creator",
  "keyword": "* "
});
formatter.step({
  "line": 9,
  "name": "def x \u003d result1.result",
  "keyword": "* "
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 34750763,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "creator",
      "offset": 4
    },
    {
      "val": "read(\u0027RequestToken.feature\u0027)",
      "offset": 14
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 16828428,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "result1",
      "offset": 4
    },
    {
      "val": "call creator",
      "offset": 14
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 52273414,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "x",
      "offset": 4
    },
    {
      "val": "result1.result",
      "offset": 8
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 261156,
  "status": "passed"
});
formatter.scenario({
  "line": 34,
  "name": "Check if the folder can be renamed",
  "description": "",
  "id": ";check-if-the-folder-can-be-renamed",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 35,
  "name": "url \u0027http://localhost:8000/OpenKM/services/OKMFolder?wsdl\u0027",
  "keyword": "Given "
});
formatter.step({
  "line": 36,
  "name": "def payload \u003d",
  "keyword": "* ",
  "doc_string": {
    "content_type": "",
    "line": 37,
    "value": "\u003csoapenv:Envelope xmlns:soapenv\u003d\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws\u003d\"http://ws.openkm.com\"\u003e\n   \u003csoapenv:Header/\u003e\n   \u003csoapenv:Body\u003e\n      \u003cws:rename\u003e\n         \u003c!--Optional:--\u003e\n         \u003ctoken\u003e#(x)\u003c/token\u003e\n         \u003c!--Optional:--\u003e\n         \u003cfldPath\u003e/okm:root/CannotReadWriteAndDelete\u003c/fldPath\u003e\n         \u003c!--Optional:--\u003e\n         \u003cnewName\u003eNewName1\u003c/newName\u003e\n      \u003c/ws:rename\u003e\n   \u003c/soapenv:Body\u003e\n\u003c/soapenv:Envelope\u003e"
  }
});
formatter.step({
  "line": 52,
  "name": "request payload",
  "keyword": "Given "
});
formatter.step({
  "line": 54,
  "name": "soap action \u0027http://ws.openkm.com/OKMFolder/rename\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "status 500",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027http://localhost:8000/OpenKM/services/OKMFolder?wsdl\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 11393347,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "payload",
      "offset": 4
    }
  ],
  "location": "StepDefs.defDocString(String,String)"
});
formatter.result({
  "duration": 10337891,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "payload",
      "offset": 8
    }
  ],
  "location": "StepDefs.request(String)"
});
formatter.result({
  "duration": 69828,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": " \u0027http://ws.openkm.com/OKMFolder/rename\u0027",
      "offset": 11
    }
  ],
  "location": "StepDefs.soapAction(String)"
});
formatter.result({
  "duration": 37463081,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "500",
      "offset": 7
    }
  ],
  "location": "StepDefs.status(int)"
});
formatter.result({
  "duration": 132958,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "url \u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
  "keyword": "* "
});
formatter.step({
  "line": 6,
  "name": "def creator \u003d read(\u0027RequestToken.feature\u0027)",
  "keyword": "* "
});
formatter.step({
  "comments": [
    {
      "line": 7,
      "value": "#  * def result \u003d callonce creator"
    }
  ],
  "line": 8,
  "name": "def result1 \u003d call creator",
  "keyword": "* "
});
formatter.step({
  "line": 9,
  "name": "def x \u003d result1.result",
  "keyword": "* "
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027http://localhost:8000/OpenKM/services/OKMAuth?wsdl\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 27518882,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "creator",
      "offset": 4
    },
    {
      "val": "read(\u0027RequestToken.feature\u0027)",
      "offset": 14
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 17097638,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "result1",
      "offset": 4
    },
    {
      "val": "call creator",
      "offset": 14
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 90609481,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "x",
      "offset": 4
    },
    {
      "val": "result1.result",
      "offset": 8
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 316684,
  "status": "passed"
});
formatter.scenario({
  "line": 59,
  "name": "Check if the folder can be deleted",
  "description": "",
  "id": ";check-if-the-folder-can-be-deleted",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 60,
  "name": "url \u0027http://localhost:8000/OpenKM/services/OKMFolder?wsdl\u0027",
  "keyword": "Given "
});
formatter.step({
  "line": 61,
  "name": "def payload \u003d",
  "keyword": "* ",
  "doc_string": {
    "content_type": "",
    "line": 62,
    "value": "\u003csoapenv:Envelope xmlns:soapenv\u003d\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws\u003d\"http://ws.openkm.com\"\u003e\n   \u003csoapenv:Header/\u003e\n   \u003csoapenv:Body\u003e\n      \u003cws:delete\u003e\n         \u003c!--Optional:--\u003e\n         \u003ctoken\u003e#(x)\u003c/token\u003e\n         \u003c!--Optional:--\u003e\n         \u003cfldPath\u003e/okm:root/CannotReadWriteAndDelete\u003c/fldPath\u003e\n      \u003c/ws:delete\u003e\n   \u003c/soapenv:Body\u003e\n\u003c/soapenv:Envelope\u003e"
  }
});
formatter.step({
  "line": 75,
  "name": "request payload",
  "keyword": "Given "
});
formatter.step({
  "line": 76,
  "name": "soap action \u0027http://ws.openkm.com/OKMFolder/delete\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 77,
  "name": "status 500",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027http://localhost:8000/OpenKM/services/OKMFolder?wsdl\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 7175679,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "payload",
      "offset": 4
    }
  ],
  "location": "StepDefs.defDocString(String,String)"
});
formatter.result({
  "duration": 8726965,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "payload",
      "offset": 8
    }
  ],
  "location": "StepDefs.request(String)"
});
formatter.result({
  "duration": 98258,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": " \u0027http://ws.openkm.com/OKMFolder/delete\u0027",
      "offset": 11
    }
  ],
  "location": "StepDefs.soapAction(String)"
});
formatter.result({
  "duration": 24320878,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "500",
      "offset": 7
    }
  ],
  "location": "StepDefs.status(int)"
});
formatter.result({
  "duration": 100212,
  "status": "passed"
});
});