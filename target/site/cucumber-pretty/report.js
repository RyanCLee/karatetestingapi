$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("sample.feature");
formatter.feature({
  "line": 1,
  "name": "",
  "description": "",
  "id": "",
  "keyword": "Feature"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "url \u0027http://www.webservicex.com/stockquote.asmx\u0027",
  "keyword": "* "
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027http://www.webservicex.com/stockquote.asmx\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 1285136197,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "get URL",
  "description": "",
  "id": ";get-url",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "request",
  "keyword": "Given ",
  "doc_string": {
    "content_type": "",
    "line": 7,
    "value": "\u003c?xml version\u003d\"1.0\" encoding\u003d\"utf-8\"?\u003e\n\u003csoap12:Envelope xmlns:xsi\u003d\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd\u003d\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12\u003d\"http://www.w3.org/2003/05/soap-envelope\"\u003e\n  \u003csoap12:Body\u003e\n    \u003cGetQuote xmlns\u003d\"http://www.webserviceX.NET/\"\u003e\n      \u003csymbol\u003egoogle.com\u003c/symbol\u003e\n    \u003c/GetQuote\u003e\n  \u003c/soap12:Body\u003e\n\u003c/soap12:Envelope\u003e"
  }
});
formatter.step({
  "line": 17,
  "name": "soap action \u0027http://www.webserviceX.NET/GetQuote\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "status 200",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "def last \u003d //GetQuoteResult",
  "keyword": "* "
});
formatter.step({
  "line": 21,
  "name": "print last",
  "keyword": "* "
});
formatter.match({
  "location": "StepDefs.requestDocString(String)"
});
formatter.result({
  "duration": 3802174,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": " \u0027http://www.webserviceX.NET/GetQuote\u0027",
      "offset": 11
    }
  ],
  "location": "StepDefs.soapAction(String)"
});
formatter.result({
  "duration": 1293344098,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 7
    }
  ],
  "location": "StepDefs.status(int)"
});
formatter.result({
  "duration": 718271,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "last",
      "offset": 4
    },
    {
      "val": "//GetQuoteResult",
      "offset": 11
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 5390613,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "last",
      "offset": 6
    }
  ],
  "location": "StepDefs.print(String\u003e)"
});
formatter.result({
  "duration": 2990469,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "url \u0027http://www.webservicex.com/stockquote.asmx\u0027",
  "keyword": "* "
});
formatter.match({
  "arguments": [
    {
      "val": "\u0027http://www.webservicex.com/stockquote.asmx\u0027",
      "offset": 4
    }
  ],
  "location": "StepDefs.url(String)"
});
formatter.result({
  "duration": 69927299,
  "status": "passed"
});
formatter.scenario({
  "line": 28,
  "name": "If the outcome is correct",
  "description": "",
  "id": ";if-the-outcome-is-correct",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 29,
  "name": "request",
  "keyword": "Given ",
  "doc_string": {
    "content_type": "",
    "line": 30,
    "value": "\u003c?xml version\u003d\"1.0\" encoding\u003d\"utf-8\"?\u003e\n\u003csoap12:Envelope xmlns:xsi\u003d\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd\u003d\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12\u003d\"http://www.w3.org/2003/05/soap-envelope\"\u003e\n\u003csoap12:Body\u003e\n\u003cGetQuote xmlns\u003d\"http://www.webserviceX.NET/\"\u003e\n  \u003csymbol\u003eINTU\u003c/symbol\u003e\n\u003c/GetQuote\u003e\n\u003c/soap12:Body\u003e\n\u003c/soap12:Envelope\u003e"
  }
});
formatter.step({
  "line": 40,
  "name": "soap action \u0027http://www.webserviceX.NET/GetQuote\u0027",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "status 200",
  "keyword": "Then "
});
formatter.step({
  "line": 43,
  "name": "def last \u003d //GetQuoteResult",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "assert last \u003d\u003d \u0027exception\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs.requestDocString(String)"
});
formatter.result({
  "duration": 2393893,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": " \u0027http://www.webserviceX.NET/GetQuote\u0027",
      "offset": 11
    }
  ],
  "location": "StepDefs.soapAction(String)"
});
formatter.result({
  "duration": 732675232,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 7
    }
  ],
  "location": "StepDefs.status(int)"
});
formatter.result({
  "duration": 75770,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "last",
      "offset": 4
    },
    {
      "val": "//GetQuoteResult",
      "offset": 11
    }
  ],
  "location": "StepDefs.def(String,String)"
});
formatter.result({
  "duration": 1938395,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "last \u003d\u003d \u0027exception\u0027",
      "offset": 7
    }
  ],
  "location": "StepDefs.asssertBoolean(String)"
});
formatter.result({
  "duration": 84602396,
  "status": "passed"
});
});