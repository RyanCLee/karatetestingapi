import com.intuit.karate.junit4.Karate;
import cucumber.api.CucumberOptions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.runner.RunWith;

@RunWith(Karate.class)
@CucumberOptions(
        format={"pretty", "html:reports/test-report"},
        features = "src/test/resources",
        glue = "steps"
)

public class Testing {
    @Given("^url$")
    public void givenStatement() {
        System.out.println("Getting the URL");
    }

    @When("^method$")
    public void whenStatement() {
        System.out.println("when statement executed successfully");
    }

    @Then("^status$")
    public void thenStatement() {
        System.out.println("done");
    }
}

/*

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Testing {

    @Given("^sample$")
    public void givenStatement() {
        System.out.println("test");
    }

    @When("^run$")
    public void whenStatement() {
        System.out.println("when statement executed successfully");
    }

    @Then("^successful$")
    public void thenStatement() {
        System.out.println("done");
    }

}

@smokeTest
Feature: test

  Scenario: setup
    Given sample
    When run
    Then successful
*/
