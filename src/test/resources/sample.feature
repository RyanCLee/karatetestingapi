@ignore
Feature:

Background:
  * url 'http://localhost:8000/OpenKM/services/OKMAuth?wsdl'
  * def creator = read('RequestToken.feature')
#  * def result = callonce creator
  * def result1 = call creator
  * def x = result1.result

  Scenario: Check if the folder can be read
    Given url 'http://localhost:8000/OpenKM/services/OKMFolder?wsdl'
    * def payload =
"""
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.openkm.com">
   <soapenv:Header/>
   <soapenv:Body>
      <ws:isValid>
         <!--Optional:-->
         <token>#(x)</token>
         <!--Optional:-->
         <fldPath>/okm:root/ReadOnly</fldPath>
      </ws:isValid>
   </soapenv:Body>
</soapenv:Envelope>
"""
    Given request payload

    When soap action 'http://ws.openkm.com/OKMFolder/isValid'
    Then status 200



      Scenario: Check if the folder can be renamed
    Given url 'http://localhost:8000/OpenKM/services/OKMFolder?wsdl'
    * def payload =
"""
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.openkm.com">
   <soapenv:Header/>
   <soapenv:Body>
      <ws:rename>
         <!--Optional:-->
         <token>#(x)</token>
         <!--Optional:-->
         <fldPath>/okm:root/CannotReadWriteAndDelete</fldPath>
         <!--Optional:-->
         <newName>NewName1</newName>
      </ws:rename>
   </soapenv:Body>
</soapenv:Envelope>
"""
    Given request payload

    When soap action 'http://ws.openkm.com/OKMFolder/rename'
    Then status 500



    Scenario: Check if the folder can be deleted
    Given url 'http://localhost:8000/OpenKM/services/OKMFolder?wsdl'
    * def payload =
"""
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.openkm.com">
   <soapenv:Header/>
   <soapenv:Body>
      <ws:delete>
         <!--Optional:-->
         <token>#(x)</token>
         <!--Optional:-->
         <fldPath>/okm:root/CannotReadWriteAndDelete</fldPath>
      </ws:delete>
   </soapenv:Body>
</soapenv:Envelope>
"""
    Given request payload
    When soap action 'http://ws.openkm.com/OKMFolder/delete'
    Then status 500

