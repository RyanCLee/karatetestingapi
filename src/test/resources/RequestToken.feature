@ignore
Feature:

 Background:
   * url 'http://localhost:8000/OpenKM/services/OKMAuth?wsdl'

Scenario: Return the token

Given url 'http://localhost:8000/OpenKM/services/OKMAuth?wsdl'
  * def payload =
"""
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.openkm.com">
   <soapenv:Header/>
   <soapenv:Body>
      <ws:login>
         <!--Optional:-->
         <user>TestUser</user>
         <!--Optional:-->
         <password>TestUser</password>
      </ws:login>
   </soapenv:Body>
</soapenv:Envelope>
"""
Given request payload

When soap action 'http://ws.openkm.com/OKMAuth/login'
Then status 200

  * def result = //return
#  * print result


#@ignore
#Feature:
#
#Scenario: Login and get Token
#
#Given url 'http://localhost:8000/OpenKM/services/OKMAuth?wsdl'
#Given request
#"""
#<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.openkm.com">
#   <soapenv:Header/>
#   <soapenv:Body>
#      <ws:login>
#         <!--Optional:-->
#         <user>TestUser</user>
#         <!--Optional:-->
#         <password>TestUser</password>
#      </ws:login>
#   </soapenv:Body>
#</soapenv:Envelope>
#"""
#When soap action 'http://ws.openkm.com/OKMAuth/login'
#Then status 200
#  * def token = //return
